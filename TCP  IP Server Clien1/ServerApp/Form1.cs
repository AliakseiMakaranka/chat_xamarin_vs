﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Media;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.BusinessLogic.Infrastructure;
using BusinessLogic.Models;
using ServerApp;
using Message = App.BusinessLogic.Models.Message;

namespace App
{
    public partial class Form1 : Form
    {
        private IProtocol _iProtocol;

        private static CancellationTokenSource _cts = new CancellationTokenSource();

        public Form1()
        {
            InitializeComponent();
        }

        private void UiOutput(Message message)       //вывод на UI
        {
            Action<Message> action = UserControlOutput;
            if (InvokeRequired)
            {
                Invoke(action, message);
            }
            else
            {
                action(message);
            }
        }

        private void UserControlOutput(Message message)     //вывод на UI
        {
            switch (message.MessageType)
            {
                case MessageType.Text:
                    var richTextBox = new RichTextBox
                    {
                        Width = 490,
                        Height = 20,
                        AutoSize = true
                    };
                    flowLayoutPanel.AutoScrollPosition = new Point(0, 10000);
                    richTextBox.Text = "Client: " + message.Text + Environment.NewLine;
                    flowLayoutPanel.Controls.Add(richTextBox);
                    break;

                case MessageType.Image:
                    var ms = new MemoryStream(message.Data);
                    var img = Image.FromStream(ms);
                    var pictureBox = new PictureBox
                    {
                        SizeMode = PictureBoxSizeMode.StretchImage,
                        Image = img
                    };
                    if (img.Size.Width <= pictureBox.Size.Width && img.Size.Height <= pictureBox.Size.Height)
                    {
                        pictureBox.Size = img.Size;
                    }
                    else
                    {
                        pictureBox.Size = new Size(200, 130);
                    }
                    flowLayoutPanel.Controls.Add(pictureBox);
                    break;

                case MessageType.Music:
                    var musicControl = new MusicControl();
                    using (new MemoryStream(message.Data))
                    {
                        var myPlayer = new SoundPlayer(new MemoryStream(message.Data));
                        musicControl.Init(myPlayer, message.Text);
                    }
                    flowLayoutPanel.Controls.Add(musicControl);
                    break;

                case MessageType.File:
                    var fileControl = new FileControl();
                    fileControl.Init(message);
                    flowLayoutPanel.Controls.Add(fileControl);
                    break;

                default:
                    Console.WriteLine("Default case");
                    break;
            }
        }
        #region handlers

        private async void button_run_server_Click(object sender, EventArgs e)
        {
            try
            {
                if (radioButton_Tcp.Checked)
                {
                    await TcpServerStart();
                }
                else if (radioButton_Udp.Checked)
                {
                    await UdpServerStart();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button_connect_Click(object sender, EventArgs e)
        {
            try
            {
                if (radioButton_Tcp.Checked)
                {
                    TcpClientStart();
                }
                else if (radioButton_Udp.Checked)
                {
                    UdpClientStart();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async Task TcpServerStart()
        {
            try
            {
                if (_iProtocol == null)
                {
                    button_run_server.Text = "Stop server";
                    _iProtocol = new TcpProtocol();
                    await _iProtocol.StartServerAsync(UiOutput, _cts.Token);
                }
                else
                {
                    button_run_server.Text = "Start server";
                    _cts.Cancel();
                    _cts.Dispose();
                    _cts = new CancellationTokenSource();
                    _iProtocol.Close();
                    _iProtocol = null;                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async Task UdpServerStart()
        {
            try
            {
                if (_iProtocol == null)
                {
                    button_run_server.Text = "Stop server";
                    _iProtocol = new UdpProtocol(IPAddress.Parse(textBox_remoteIp.Text));
                    await _iProtocol.StartServerAsync(UiOutput, _cts.Token);
                }
                else
                {
                    button_run_server.Text = "Start server";
                    _cts.Cancel();
                    _cts.Dispose();
                    _cts = new CancellationTokenSource();
                    _iProtocol.Close();
                    _iProtocol = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void TcpClientStart()
        {
            try
            {
                if (_iProtocol == null)
                {
                    button_connect.Text = "Disconect";
                    _iProtocol = new TcpProtocol(IPAddress.Parse(textBox_remoteIp.Text));
                    await _iProtocol.StartClientAsync(UiOutput, _cts.Token);
                }
                else
                {
                    _cts.Cancel();
                    _cts.Dispose();
                    _cts = new CancellationTokenSource();
                    _iProtocol.Close();
                    _iProtocol = null;
                    button_connect.Text = "Connect";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void UdpClientStart()
        {
            try
            {
                _iProtocol = new UdpProtocol(IPAddress.Parse(textBox_remoteIp.Text));
                await _iProtocol.StartClientAsync(UiOutput, _cts.Token);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox_message_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    Message message = new Message
                    {
                        MessageType = MessageType.Text,
                        Text = textBox_message.Text
                    };
                    //вывод текста эхо
                    RichTextBox richTextBox = new RichTextBox();
                    richTextBox.Width = 490;
                    richTextBox.Height = 20;
                    richTextBox.AutoSize = true;
                    flowLayoutPanel.AutoScrollPosition = new Point(0, 10000);
                    richTextBox.Text = "Server: " + message.Text + Environment.NewLine;
                    richTextBox.BackColor = Color.Lavender;
                    flowLayoutPanel.Controls.Add(richTextBox);
                    _iProtocol.Send(message);
                    textBox_message.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button_choose_image_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog
            {
                Title = "Open Image",
                Filter = "bmp files (*.bmp)|*.bmp"
            };
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                Message message = new Message();
                message.MessageType = MessageType.Image;
                message.Text = dlg.SafeFileName;
                message.Data = File.ReadAllBytes(dlg.InitialDirectory + dlg.FileName);
                _iProtocol.Send(message);
            }
            dlg.Dispose();
        }

        private void button_music_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog
            {
                Title = "Open music",
                Filter = "WAV Files (*.wav)|*.wav"
            };
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                Message message = new Message();
                message.MessageType = MessageType.Music;
                message.Text = dlg.SafeFileName;
                message.Data = File.ReadAllBytes(dlg.InitialDirectory + dlg.FileName);
                _iProtocol.Send(message);
            }
            dlg.Dispose();
        }

        private void button_file_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog
            {
                Title = "Open any file",
                Filter = "Any Files (*.*)|*.*"
            };

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                Message message = new Message();
                message.MessageType = MessageType.File;
                message.Text = dlg.SafeFileName;
                message.Data = File.ReadAllBytes(dlg.InitialDirectory + dlg.FileName);
                try
                {
                    _iProtocol.Send(message);
                }
                catch (NullReferenceException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            dlg.Dispose();
        }

        private void button_clear_Click(object sender, EventArgs e)
        {
            foreach (Control control in flowLayoutPanel.Controls)
            {
                flowLayoutPanel.Controls.Remove(control);
                control.Dispose();
            }
        }

        private void button_quit_Click(object sender, EventArgs e)
        {
            Process.GetCurrentProcess().Kill();
            Application.Exit();
        }
        #endregion
    }
}
