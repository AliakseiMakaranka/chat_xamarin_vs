﻿using System;
using BusinessLogic.Models;

namespace App.BusinessLogic.Models
{
    [Serializable]
    public class Message
    {
        public MessageType MessageType { get; set; }

        public byte[] Data { get; set; }

        public string Text { get; set; }
    }
}
