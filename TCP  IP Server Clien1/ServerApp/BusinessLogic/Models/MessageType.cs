﻿namespace BusinessLogic.Models
{
    public enum MessageType
    {
        Text = 1,
        Image,
        Music,
        File
    }
}
