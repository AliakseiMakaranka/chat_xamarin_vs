﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Message = App.BusinessLogic.Models.Message;

namespace App.BusinessLogic.Infrastructure
{
    public class TcpProtocol : IProtocol
    {
        private Action<Message> _action;        //хранится ссылка на метод UiOutput

        readonly Serealizer _serealizer;

        private TcpClient _client;

        private readonly TcpListener _server;

        private int Port { get; } = 3000;

        private IPAddress RemoteIpAddress { get; }

        private IPEndPoint _ipLocalEndPoint;

        private NetworkStream _readStream;

        private byte[] _data = new Byte[15000000];

        public TcpProtocol()
        {
            _serealizer = new Serealizer();

            //Создаем ЛОКАЛЬНУЮ точку
            _ipLocalEndPoint = new IPEndPoint(GetLocalIp(), Port);

            // Создаем "слушателя" для указанного порта
            _server = new TcpListener(_ipLocalEndPoint);
        }

        public TcpProtocol(IPAddress remoteIp) : this()
        {
            RemoteIpAddress = remoteIp;
        }

        public Task StartServerAsync(Action<Message> action, CancellationToken token)
        {
            return StartServer(action, token);
        }


        private async Task StartServer(Action<Message> action, CancellationToken token)
        {
            _action = action;

            _server.Start();

            try
            {
                _client = await _server.AcceptTcpClientAsync();

                _readStream = _client?.GetStream();

                await Task.Run(() =>
                {
                    while (token.IsCancellationRequested == false)
                    {
                        Task.Delay(1).Wait();
                        while (_readStream.DataAvailable && _readStream != null)
                        {
                            _readStream.Read(_data, 0, _data.Length);
                            Serealizer serealizer = new Serealizer();
                            var deserializeMessage = serealizer.Deserialize(_data);
                            _action(deserializeMessage);
                        }
                    }
                });
            }
            catch (ObjectDisposedException)
            {
                MessageBox.Show("Операция отменена пользователем");
            }
        }

        public Task StartClientAsync(Action<Message> action, CancellationToken token)
        {
            return StartClient(action, token);
        }

        private async Task StartClient(Action<Message> action, CancellationToken token)
        {
            _action = action;
            //Создаем ЛОКАЛЬНУЮ точку
            _ipLocalEndPoint = new IPEndPoint(GetLocalIp(), Port);

            try
            {
                _client = new TcpClient(_ipLocalEndPoint);
                _client.Connect(RemoteIpAddress, Port);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            //Создаем поток для чтения и отсылки инфы на сервер
            _readStream = _client?.GetStream();

            await Task.Run(() =>
            {
                while (token.IsCancellationRequested == false)
                {
                    Task.Delay(1).Wait();
                    while (_readStream.DataAvailable && _readStream != null)
                    {
                        int bytesread = _readStream.Read(_data, 0, _data.Length);

                        if (bytesread > 0)
                        {
                            var deserializeMessage = _serealizer.Deserialize(_data);
                            _action(deserializeMessage);
                        }
                    }
                }
            });
        }

        public void Close()
        {
            try
            {
                _server?.Stop();
                _client?.Close();
                _readStream?.Close();

                //_client?.Client.Disconnect(true);    //виснет
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Send(Message message)
        {
            try
            {
                var serializeMessage = _serealizer.Serialize(message);
                _readStream.Write(serializeMessage, 0, serializeMessage.Length);
                _readStream.Flush();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static IPAddress GetLocalIp()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            return host.AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }
    }
}
