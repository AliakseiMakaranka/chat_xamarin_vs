﻿using System;
using System.Threading;
using System.Threading.Tasks;
using App.BusinessLogic.Models;
using BusinessLogic.Models;

namespace App.BusinessLogic.Infrastructure
{
    public interface IProtocol
    {
        void Send(Message message);

        Task StartServerAsync(Action<Message> action, CancellationToken token);

        Task StartClientAsync(Action<Message> action, CancellationToken token);

        void Close();
    }
}
