﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using App.BusinessLogic.Models;
using BusinessLogic.Models;

namespace App.BusinessLogic.Infrastructure
{
    public class Serealizer
    {
        public byte[] Serialize(Message message)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                var binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(ms, message);
                var data = ms.ToArray();
                return data;
            }
        }

        public Message Deserialize(byte[] data)
        {
            using (MemoryStream ms = new MemoryStream(data))
            {
                var binaryFormatter = new BinaryFormatter();
                var objDeserialized = binaryFormatter.Deserialize(ms) as Message;
                return objDeserialized;
            }
        }
    }
}
