﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Message = App.BusinessLogic.Models.Message;

namespace App.BusinessLogic.Infrastructure
{
    public class UdpProtocol : IProtocol
    {
        private Action<Message> _action;        //хранится ссылка на метод UiOutput

        private readonly Serealizer _serealizer;

        private readonly UdpClient _connection;

        private int Port { get; } = 3000;

        private IPAddress RemoteIpAddress { get; }

        public UdpProtocol(IPAddress remoteIp)
        {
            _connection = new UdpClient(Port);
            _serealizer = new Serealizer();
            RemoteIpAddress = remoteIp;
        }

        public Task StartServerAsync(Action<Message> action, CancellationToken token)
        {
            return StartServer(action, token);
        }

        private async Task StartServer(Action<Message> action, CancellationToken token)
        {
            _action = action;
            UdpReceiveResult receiveBytes;

            await Task.Run(async () =>
            {
                while (token.IsCancellationRequested == false)
                {
                    try
                    {
                        Task.Delay(100);
                        receiveBytes = await _connection.ReceiveAsync();
                        var deserializeMessage = _serealizer.Deserialize(receiveBytes.Buffer);
                        _action(deserializeMessage);
                    }
                    catch (ObjectDisposedException)
                    {
                        MessageBox.Show("Операция отменена пользователем");
                    }
                }
            });
        }

        public Task StartClientAsync(Action<Message> action, CancellationToken token)
        {
            StartServerAsync(action, token);        //тут совпадает, но Интерфейс требует реализации, поэтому перенаправляем на StartServerAsync
            return Task.FromResult(0);
        }

        public void Close()
        {
            try
            {
                _connection?.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Send(Message message)
        {
            var sender = new UdpClient();
            // Создаем endPoint по информации об удаленном хосте
            var endPoint = new IPEndPoint(RemoteIpAddress, Port);
            var serializeMessage = _serealizer.Serialize(message);
            sender.Send(serializeMessage, serializeMessage.Length, endPoint);
            sender.Close();
        }
    }
}
