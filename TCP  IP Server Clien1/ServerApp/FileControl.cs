﻿using System;
using System.IO;
using System.Windows.Forms;
using Message = App.BusinessLogic.Models.Message;

namespace ServerApp
{
    public partial class FileControl : UserControl
    {
        Message _message;
        string _name;

        public FileControl()
        {
            InitializeComponent();
        }

        public void Init(Message message)
        {
            _message = message;
            _name = _message.Text;
            label_download_file_name.Text = message.Text;
        }

        private void button_download_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog
            {
                FileName = _name,
                Filter = "files (*.*)|*.*|All files (*.*)|*.*",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (BinaryWriter writer = new BinaryWriter(File.Open(saveFileDialog1.FileName, FileMode.Create)))
                    {
                        writer.Write(_message.Data);
                        writer.Flush();
                        writer.Close();
                        label_file_status.Text = "Successful done!";
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    label_file_status.Text = "Error!!!";
                }
            }

        }
    }
}
