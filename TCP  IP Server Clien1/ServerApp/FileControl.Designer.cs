﻿namespace ServerApp
{
    partial class FileControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_download = new System.Windows.Forms.Button();
            this.label_download_file_name = new System.Windows.Forms.Label();
            this.label_file_status = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_download
            // 
            this.button_download.Location = new System.Drawing.Point(12, 8);
            this.button_download.Name = "button_download";
            this.button_download.Size = new System.Drawing.Size(75, 23);
            this.button_download.TabIndex = 0;
            this.button_download.Text = "Download";
            this.button_download.UseVisualStyleBackColor = true;
            this.button_download.Click += new System.EventHandler(this.button_download_Click);
            // 
            // label_download_file_name
            // 
            this.label_download_file_name.AutoSize = true;
            this.label_download_file_name.Location = new System.Drawing.Point(95, 13);
            this.label_download_file_name.Name = "label_download_file_name";
            this.label_download_file_name.Size = new System.Drawing.Size(0, 13);
            this.label_download_file_name.TabIndex = 1;
            // 
            // label_file_status
            // 
            this.label_file_status.AutoSize = true;
            this.label_file_status.Location = new System.Drawing.Point(16, 34);
            this.label_file_status.Name = "label_file_status";
            this.label_file_status.Size = new System.Drawing.Size(0, 13);
            this.label_file_status.TabIndex = 2;
            // 
            // FileControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label_file_status);
            this.Controls.Add(this.label_download_file_name);
            this.Controls.Add(this.button_download);
            this.Name = "FileControl";
            this.Size = new System.Drawing.Size(300, 50);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_download;
        private System.Windows.Forms.Label label_download_file_name;
        private System.Windows.Forms.Label label_file_status;
    }
}
