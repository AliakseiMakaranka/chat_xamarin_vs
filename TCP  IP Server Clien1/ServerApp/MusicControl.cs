﻿using System;
using System.Media;
using System.Windows.Forms;

namespace ServerApp
{
    public partial class MusicControl : UserControl
    {
        SoundPlayer _myPlayer;

        public MusicControl()
        {
            InitializeComponent();
        }

        public void Init(SoundPlayer myPlayer, string songName)
        {
            _myPlayer = myPlayer;
            label_music_name.Text = songName;
        }

        private void button_play_Click(object sender, EventArgs e)
        {
            _myPlayer.Play();
        }

        private void button_stop_Click(object sender, EventArgs e)
        {
            _myPlayer.Stop();
        }
    }
}
