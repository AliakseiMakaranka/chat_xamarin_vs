﻿namespace App
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_run_server = new System.Windows.Forms.Button();
            this.textBox_message = new System.Windows.Forms.TextBox();
            this.button_clear = new System.Windows.Forms.Button();
            this.button_quit = new System.Windows.Forms.Button();
            this.button_choose_image = new System.Windows.Forms.Button();
            this.button_choose_music = new System.Windows.Forms.Button();
            this.button_choose_file = new System.Windows.Forms.Button();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton_Udp = new System.Windows.Forms.RadioButton();
            this.radioButton_Tcp = new System.Windows.Forms.RadioButton();
            this.textBox_remoteIp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button_connect = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_run_server
            // 
            this.button_run_server.Location = new System.Drawing.Point(362, 14);
            this.button_run_server.Name = "button_run_server";
            this.button_run_server.Size = new System.Drawing.Size(74, 23);
            this.button_run_server.TabIndex = 4;
            this.button_run_server.Text = "Run Server";
            this.button_run_server.UseVisualStyleBackColor = true;
            this.button_run_server.Click += new System.EventHandler(this.button_run_server_Click);
            // 
            // textBox_message
            // 
            this.textBox_message.Location = new System.Drawing.Point(12, 405);
            this.textBox_message.Name = "textBox_message";
            this.textBox_message.Size = new System.Drawing.Size(506, 20);
            this.textBox_message.TabIndex = 6;
            this.textBox_message.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_message_KeyDown);
            // 
            // button_clear
            // 
            this.button_clear.Location = new System.Drawing.Point(362, 431);
            this.button_clear.Name = "button_clear";
            this.button_clear.Size = new System.Drawing.Size(75, 23);
            this.button_clear.TabIndex = 7;
            this.button_clear.Text = "Clear";
            this.button_clear.UseVisualStyleBackColor = true;
            this.button_clear.Click += new System.EventHandler(this.button_clear_Click);
            // 
            // button_quit
            // 
            this.button_quit.Location = new System.Drawing.Point(443, 430);
            this.button_quit.Name = "button_quit";
            this.button_quit.Size = new System.Drawing.Size(75, 23);
            this.button_quit.TabIndex = 9;
            this.button_quit.Text = "Quit";
            this.button_quit.UseVisualStyleBackColor = true;
            this.button_quit.Click += new System.EventHandler(this.button_quit_Click);
            // 
            // button_choose_image
            // 
            this.button_choose_image.Location = new System.Drawing.Point(12, 431);
            this.button_choose_image.Name = "button_choose_image";
            this.button_choose_image.Size = new System.Drawing.Size(35, 20);
            this.button_choose_image.TabIndex = 10;
            this.button_choose_image.Text = "img";
            this.button_choose_image.UseVisualStyleBackColor = true;
            this.button_choose_image.Click += new System.EventHandler(this.button_choose_image_Click);
            // 
            // button_choose_music
            // 
            this.button_choose_music.Location = new System.Drawing.Point(53, 431);
            this.button_choose_music.Name = "button_choose_music";
            this.button_choose_music.Size = new System.Drawing.Size(35, 20);
            this.button_choose_music.TabIndex = 11;
            this.button_choose_music.Text = "wav";
            this.button_choose_music.UseVisualStyleBackColor = true;
            this.button_choose_music.Click += new System.EventHandler(this.button_music_Click);
            // 
            // button_choose_file
            // 
            this.button_choose_file.Location = new System.Drawing.Point(94, 431);
            this.button_choose_file.Name = "button_choose_file";
            this.button_choose_file.Size = new System.Drawing.Size(30, 20);
            this.button_choose_file.TabIndex = 12;
            this.button_choose_file.Text = "file";
            this.button_choose_file.UseVisualStyleBackColor = true;
            this.button_choose_file.Click += new System.EventHandler(this.button_file_Click);
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.AutoScroll = true;
            this.flowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel.Location = new System.Drawing.Point(16, 46);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(500, 350);
            this.flowLayoutPanel.TabIndex = 13;
            this.flowLayoutPanel.WrapContents = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton_Udp);
            this.groupBox1.Controls.Add(this.radioButton_Tcp);
            this.groupBox1.Location = new System.Drawing.Point(16, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(179, 33);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Protocol";
            // 
            // radioButton_Udp
            // 
            this.radioButton_Udp.AutoSize = true;
            this.radioButton_Udp.Location = new System.Drawing.Point(117, 10);
            this.radioButton_Udp.Name = "radioButton_Udp";
            this.radioButton_Udp.Size = new System.Drawing.Size(45, 17);
            this.radioButton_Udp.TabIndex = 1;
            this.radioButton_Udp.Text = "Udp";
            this.radioButton_Udp.UseVisualStyleBackColor = true;
            // 
            // radioButton_Tcp
            // 
            this.radioButton_Tcp.AutoSize = true;
            this.radioButton_Tcp.Checked = true;
            this.radioButton_Tcp.Location = new System.Drawing.Point(67, 10);
            this.radioButton_Tcp.Name = "radioButton_Tcp";
            this.radioButton_Tcp.Size = new System.Drawing.Size(44, 17);
            this.radioButton_Tcp.TabIndex = 0;
            this.radioButton_Tcp.TabStop = true;
            this.radioButton_Tcp.Text = "Tcp";
            this.radioButton_Tcp.UseVisualStyleBackColor = true;
            // 
            // textBox_remoteIp
            // 
            this.textBox_remoteIp.Location = new System.Drawing.Point(255, 16);
            this.textBox_remoteIp.Name = "textBox_remoteIp";
            this.textBox_remoteIp.Size = new System.Drawing.Size(102, 20);
            this.textBox_remoteIp.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(201, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Enter IP :";
            // 
            // button_connect
            // 
            this.button_connect.Location = new System.Drawing.Point(443, 14);
            this.button_connect.Name = "button_connect";
            this.button_connect.Size = new System.Drawing.Size(73, 23);
            this.button_connect.TabIndex = 17;
            this.button_connect.Text = "Connect";
            this.button_connect.UseVisualStyleBackColor = true;
            this.button_connect.Click += new System.EventHandler(this.button_connect_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 457);
            this.Controls.Add(this.textBox_remoteIp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_connect);
            this.Controls.Add(this.button_run_server);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.flowLayoutPanel);
            this.Controls.Add(this.button_choose_file);
            this.Controls.Add(this.button_choose_music);
            this.Controls.Add(this.button_choose_image);
            this.Controls.Add(this.button_quit);
            this.Controls.Add(this.button_clear);
            this.Controls.Add(this.textBox_message);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "Chat";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button_run_server;
        private System.Windows.Forms.TextBox textBox_message;
        private System.Windows.Forms.Button button_clear;
        private System.Windows.Forms.Button button_quit;
        private System.Windows.Forms.Button button_choose_image;
        private System.Windows.Forms.Button button_choose_music;
        private System.Windows.Forms.Button button_choose_file;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton_Udp;
        private System.Windows.Forms.RadioButton radioButton_Tcp;
        private System.Windows.Forms.TextBox textBox_remoteIp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_connect;
    }
}

