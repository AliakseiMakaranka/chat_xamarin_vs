﻿using System;
using System.Media;

namespace Server
{
	[System.ComponentModel.ToolboxItem(true)]
	public partial class MusicControl : Gtk.Bin
	{
		SoundPlayer _myPlayer;

		public MusicControl()
		{
			this.Build();
		}

		public void Init(SoundPlayer myPlayer, string songName)
		{
			_myPlayer = myPlayer;
			label_music_name.Text = songName;
		}

		protected void PlayClicked(object sender, EventArgs e)
		{
			_myPlayer.Play();
		}

		protected void StopClicked(object sender, EventArgs e)
		{
			_myPlayer.Stop();
		}
	}
}
