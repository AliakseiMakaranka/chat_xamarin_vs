﻿using System;
using Gtk;
using BusinessLogic.Infrastructure;
using Message = BusinessLogic.Models.Message;
using System.Diagnostics;
using System.IO;
using System.Media;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

//server
public partial class MainWindow : Gtk.Window
{
	public string TextBoxMessage { get; set; }

	private int _port = 3000;

	Thread _listenThread;
	IProtocol _iProtocol;
	Box box;

	public MainWindow() : base(Gtk.WindowType.Toplevel)
	{
		Build();
		textBox_remoteIp.Text="192.168.74.224";
		SetSizeRequest(300, 280);
		box = new VBox(false, 0);
		scrolledwindow1.Add(createScrolledWindow(box));
		scrolledwindow1.ShowAll();
	}

	private static Widget createScrolledWindow(Widget child)
	{
		ScrolledWindow scrolledWindow = new ScrolledWindow();
		scrolledWindow.SetPolicy(PolicyType.Never, PolicyType.Automatic);
		scrolledWindow.Add(doWorkaround(child));

		return scrolledWindow;
	}

	private static Viewport doWorkaround(Widget child)
	{
		Viewport viewport = new Viewport();
		viewport.Add(child);
		child.SizeRequested += (o, args) =>
		{
			viewport.WidthRequest = viewport.Child.Requisition.Width;
		};

		return viewport;
	}

	private ThreadStart UiOutput(Message message)       //вывод на UI
	{
		UserControlOutput(message);
		return null;
	}

	private void UserControlOutput(Message message)		//вывод на UI
	{
		switch (message.MessageType)
		{
			case BusinessLogic.Models.MessageType.Text:
			TextView textView = new TextView
			{
				WidthRequest = 490,
				HeightRequest = 20,
			};
			AddLabel("Client: " + message.Text);
			break;

			case BusinessLogic.Models.MessageType.Image:
			MemoryStream ms = new MemoryStream(message.Data);
			Image pictureBox = new Image(new Gdk.Pixbuf(ms));
			pictureBox.WidthRequest = 200;
			pictureBox.HeightRequest = 130;
			AddImage(pictureBox);
			break;

			case BusinessLogic.Models.MessageType.Music:
			Server.MusicControl musicControl = new Server.MusicControl();

			using (new MemoryStream(message.Data))
			{
				SoundPlayer myPlayer = new SoundPlayer(new MemoryStream(message.Data));
				musicControl.Init(myPlayer, message.Text);
			}
			AddMusic(musicControl);
			break;

			case BusinessLogic.Models.MessageType.File:
			Server.FileControl fileControl = new Server.FileControl();
			fileControl.Init(message);
			AddFile(fileControl);
			break;

			default:
			Console.WriteLine("Default case");
			break;
		}
	}

	private void AddLabel(string str)
	{
		Label lbl = new Label(str);
		box.PackStart(lbl, false, false, 5);
		box.ShowAll();
	}

	private void AddImage(Image pictureBox)
	{
		box.PackStart(pictureBox, false, false, 5);
		box.ShowAll();
	}

	private void AddMusic(Server.MusicControl musicControl)
	{
		box.PackStart(musicControl, false, false, 5);
		box.ShowAll();
	}

	private void AddFile(Server.FileControl fileControl)
	{
		box.PackStart(fileControl, false, false, 5);
		box.ShowAll();
	}

	protected void RunServerClicked(object sender, EventArgs e)
	{
		try
		{
			if (radioButton_Tcp.Active)
			{
				button_choose_image.Visible=true;
				button_choose_music1.Visible=true;
				button_choose_file.Visible=true;

				_iProtocol = new TcpProtocol(IPAddress.Any, _port);  //передаем наш IP
				Task.Run(() =>
				{
					_listenThread = new Thread(_iProtocol.InitServer(message => UiOutput(message)));
					_listenThread.Start();
				});
			}
			else if (radioButton_Udp.Active)
			{
				button_choose_image.Visible=false;
				button_choose_music1.Visible=false;
				button_choose_file.Visible=false;

				_iProtocol = new UdpProtocol(IPAddress.Parse(textBox_remoteIp.Text), _port);  //передаем удаленный IP
				Task.Run(() =>
				{
					_listenThread = new Thread(_iProtocol.InitServer(message => UiOutput(message)));
					_listenThread.Start();
				});
			}
		}
		catch (Exception ex)
		{
			MessageDialog md = new MessageDialog(null, DialogFlags.Modal, Gtk.MessageType.Info, ButtonsType.Ok, ex.Message);
			md.Run();
			md.Destroy();
		}
	}

	[GLib.ConnectBefore]
	protected void keyPressEvent(object o, KeyPressEventArgs args)
	{
		if (args.Event.Key == Gdk.Key.Return)
		{
			Message message = new Message
			{
				MessageType = BusinessLogic.Models.MessageType.Text,
				Text = textBox_message.Text
			};
			try
			{
				_iProtocol.Send(message);
			}
			catch (Exception ex)
			{
				MessageDialog md = new MessageDialog(null, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, ex.Message);
				md.Run();
				md.Destroy();
			}
			//вывод текста эхо
			AddLabel("Server:" + message.Text);
			textBox_message.Text = string.Empty;
		}
	}

	protected void img_clicked(object sender, EventArgs e)
	{
		try
		{
			FileChooserDialog dlg =
				new FileChooserDialog("Choose the image to open",
										  this,
										  FileChooserAction.Open,
									  Stock.Cancel, ResponseType.Cancel,
									  Stock.Open, ResponseType.Accept);

			FileFilter filter = new FileFilter();
			filter.Name = "Bmp images";
			filter.AddMimeType("image/bmp");
			filter.AddPattern("*.bmp");
			dlg.AddFilter(filter);

			if (dlg.Run() == (int) ResponseType.Accept)
			{
				Message message = new Message
				{
					MessageType = BusinessLogic.Models.MessageType.Image,
					Text = dlg.Filename,
					Data = File.ReadAllBytes(dlg.Filename)
				};

				if (message.Data.Length < 8192)
					_iProtocol.Send(message);
				else
				{
					MessageDialog md = new MessageDialog(null, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, "Too large image");
					md.Run();
					md.Destroy();
				}
			}
			dlg.Destroy();
		}
		catch (Exception ex)
		{
			MessageDialog md = new MessageDialog(null, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, ex.Message);
			md.Run();
			md.Destroy();
		}
	}

	protected void wav_clicked(object sender, EventArgs e)
	{
		FileChooserDialog dlg =
			new FileChooserDialog("Choose the music to open",
								  this,
								  FileChooserAction.Open,
								  Stock.Cancel, ResponseType.Cancel,
								  Stock.Open, ResponseType.Accept);

		FileFilter filter = new FileFilter();
		filter.Name = "wav music";
		filter.AddMimeType("music/wav");
		filter.AddPattern("*.wav");
		dlg.AddFilter(filter);

		if (dlg.Run() == (int) ResponseType.Accept)
		{
			Message message = new Message
			{
				MessageType = BusinessLogic.Models.MessageType.Music,
				Text = dlg.Filename,
				Data = File.ReadAllBytes(dlg.Filename)
			};
			_iProtocol.Send(message);
		}
		dlg.Destroy();
	}

	protected void file_clicked(object sender, EventArgs e)
	{
		FileChooserDialog dlg =
			new FileChooserDialog("Choose the file to open",
								  this,
								  FileChooserAction.Open,
								  Stock.Cancel, ResponseType.Cancel,
								  Stock.Open, ResponseType.Accept);

		FileFilter filter = new FileFilter();
		filter.Name = "Any files";
		filter.AddMimeType("file/any");
		filter.AddPattern("*.*");
		dlg.AddFilter(filter);

		if (dlg.Run() == (int) ResponseType.Accept)
		{
			Message message = new Message
			{
				MessageType = BusinessLogic.Models.MessageType.File,
				Text = System.IO.Path.GetFileName(dlg.Filename),
				Data = File.ReadAllBytes(dlg.Filename)
			};
			_iProtocol.Send(message);
		}
		dlg.Destroy();
	}

	protected void ClearClicked(object sender, EventArgs e)
	{
		foreach (var x in box.Children)
		{
			box.Remove(x);
		}
	}

	protected void QuitClicked(object sender, EventArgs e)
	{
		Process.GetCurrentProcess().Kill();
		Application.Quit();
	}

	protected void OnDeleteEvent(object sender, DeleteEventArgs a)
	{
		Application.Quit();
		a.RetVal = true;
	}

}