﻿using System;
using System.IO;
using Message = BusinessLogic.Models.Message;
using Gtk;

namespace Client
{
	[System.ComponentModel.ToolboxItem(true)]
	public partial class FileControl : Gtk.Bin
	{
		private Message _message;
		private string _name;
		Window _parent;

		public FileControl()
		{
			this.Build();
		}

		public void Init(Message message)
		{
			_message = message;
			_name = _message.Text;
			label_file_status.Text = message.Text;
		}

		protected void button_download_Clicked(object sender, EventArgs e)
		{
			try
			{
				FileChooserDialog dlg = new FileChooserDialog(
					"Choose directory for save file"
					, _parent
					, FileChooserAction.Save
					, "Отмена", ResponseType.Cancel
					, "Выбрать", ResponseType.Accept);
				
				dlg.SetCurrentFolder(Environment.CurrentDirectory);

				if (dlg.Run() == (int) ResponseType.Accept)
				{
					var folderToSave = dlg.CurrentFolder + "\\" + _message.Text;
					//var folderToSave = dlg.Filename;
					using (FileStream fs = File.Create(folderToSave)) 
					{
						fs.Write(_message.Data,0,_message.Data.Length);
					}
					label_file_status.Text = "Save done!!!";
				}
				dlg.Destroy();
			}
			catch (Exception ex)
			{
				MessageDialog md = new MessageDialog(null, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, ex.Message);
				md.Run();
				md.Destroy();
				label_file_status.Text = "Error!!!";
			}
		}
	}
}
