
// This file has been generated by the GUI designer. Do not modify.

public partial class MainWindow
{
	private global::Gtk.VBox vbox3;

	private global::Gtk.HBox hbox8;

	private global::Gtk.HBox hbox9;

	private global::Gtk.Label label1;

	private global::Gtk.Entry textBox_remoteIp;

	private global::Gtk.HBox hbox10;

	private global::Gtk.RadioButton radioButton_Tcp;

	private global::Gtk.RadioButton radioButtonUdp;

	private global::Gtk.Button button_connect;

	private global::Gtk.ScrolledWindow scrolledwindow1;

	private global::Gtk.VBox vbox4;

	private global::Gtk.Entry textBox_message;

	private global::Gtk.HBox hbox11;

	private global::Gtk.HBox hbox12;

	private global::Gtk.Button button_choose_image;

	private global::Gtk.Button button_choose_music;

	private global::Gtk.Button button_choose_file;

	private global::Gtk.HBox hbox13;

	private global::Gtk.Button button_clear;

	private global::Gtk.Button button_quit;

	protected virtual void Build()
	{
		global::Stetic.Gui.Initialize(this);
		// Widget MainWindow
		this.WidthRequest = 300;
		this.HeightRequest = 200;
		this.Name = "MainWindow";
		this.Title = global::Mono.Unix.Catalog.GetString("MainWindow");
		this.WindowPosition = ((global::Gtk.WindowPosition)(4));
		// Container child MainWindow.Gtk.Container+ContainerChild
		this.vbox3 = new global::Gtk.VBox();
		this.vbox3.Name = "vbox3";
		this.vbox3.Spacing = 6;
		// Container child vbox3.Gtk.Box+BoxChild
		this.hbox8 = new global::Gtk.HBox();
		this.hbox8.Name = "hbox8";
		this.hbox8.Spacing = 6;
		// Container child hbox8.Gtk.Box+BoxChild
		this.hbox9 = new global::Gtk.HBox();
		this.hbox9.Name = "hbox9";
		this.hbox9.Spacing = 6;
		// Container child hbox9.Gtk.Box+BoxChild
		this.label1 = new global::Gtk.Label();
		this.label1.Name = "label1";
		this.label1.LabelProp = global::Mono.Unix.Catalog.GetString("Enter IP:");
		this.hbox9.Add(this.label1);
		global::Gtk.Box.BoxChild w1 = ((global::Gtk.Box.BoxChild)(this.hbox9[this.label1]));
		w1.Position = 0;
		w1.Expand = false;
		w1.Fill = false;
		// Container child hbox9.Gtk.Box+BoxChild
		this.textBox_remoteIp = new global::Gtk.Entry();
		this.textBox_remoteIp.CanFocus = true;
		this.textBox_remoteIp.Events = ((global::Gdk.EventMask)(8192));
		this.textBox_remoteIp.Name = "textBox_remoteIp";
		this.textBox_remoteIp.IsEditable = true;
		this.textBox_remoteIp.InvisibleChar = '●';
		this.hbox9.Add(this.textBox_remoteIp);
		global::Gtk.Box.BoxChild w2 = ((global::Gtk.Box.BoxChild)(this.hbox9[this.textBox_remoteIp]));
		w2.Position = 1;
		this.hbox8.Add(this.hbox9);
		global::Gtk.Box.BoxChild w3 = ((global::Gtk.Box.BoxChild)(this.hbox8[this.hbox9]));
		w3.Position = 0;
		// Container child hbox8.Gtk.Box+BoxChild
		this.hbox10 = new global::Gtk.HBox();
		this.hbox10.Name = "hbox10";
		this.hbox10.Spacing = 6;
		// Container child hbox10.Gtk.Box+BoxChild
		this.radioButton_Tcp = new global::Gtk.RadioButton(global::Mono.Unix.Catalog.GetString("Tcp"));
		this.radioButton_Tcp.CanFocus = true;
		this.radioButton_Tcp.Name = "radioButton_Tcp";
		this.radioButton_Tcp.DrawIndicator = true;
		this.radioButton_Tcp.UseUnderline = true;
		this.radioButton_Tcp.Group = new global::GLib.SList(global::System.IntPtr.Zero);
		this.hbox10.Add(this.radioButton_Tcp);
		global::Gtk.Box.BoxChild w4 = ((global::Gtk.Box.BoxChild)(this.hbox10[this.radioButton_Tcp]));
		w4.Position = 0;
		// Container child hbox10.Gtk.Box+BoxChild
		this.radioButtonUdp = new global::Gtk.RadioButton(global::Mono.Unix.Catalog.GetString("Udp"));
		this.radioButtonUdp.CanFocus = true;
		this.radioButtonUdp.Name = "radioButtonUdp";
		this.radioButtonUdp.DrawIndicator = true;
		this.radioButtonUdp.UseUnderline = true;
		this.radioButtonUdp.Group = this.radioButton_Tcp.Group;
		this.hbox10.Add(this.radioButtonUdp);
		global::Gtk.Box.BoxChild w5 = ((global::Gtk.Box.BoxChild)(this.hbox10[this.radioButtonUdp]));
		w5.Position = 1;
		this.hbox8.Add(this.hbox10);
		global::Gtk.Box.BoxChild w6 = ((global::Gtk.Box.BoxChild)(this.hbox8[this.hbox10]));
		w6.Position = 1;
		// Container child hbox8.Gtk.Box+BoxChild
		this.button_connect = new global::Gtk.Button();
		this.button_connect.CanFocus = true;
		this.button_connect.Name = "button_connect";
		this.button_connect.UseUnderline = true;
		this.button_connect.Label = global::Mono.Unix.Catalog.GetString("Connect");
		this.hbox8.Add(this.button_connect);
		global::Gtk.Box.BoxChild w7 = ((global::Gtk.Box.BoxChild)(this.hbox8[this.button_connect]));
		w7.Position = 2;
		w7.Expand = false;
		w7.Fill = false;
		this.vbox3.Add(this.hbox8);
		global::Gtk.Box.BoxChild w8 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox8]));
		w8.Position = 0;
		w8.Expand = false;
		w8.Fill = false;
		// Container child vbox3.Gtk.Box+BoxChild
		this.scrolledwindow1 = new global::Gtk.ScrolledWindow();
		this.scrolledwindow1.CanFocus = true;
		this.scrolledwindow1.Name = "scrolledwindow1";
		this.scrolledwindow1.HscrollbarPolicy = ((global::Gtk.PolicyType)(2));
		this.scrolledwindow1.ShadowType = ((global::Gtk.ShadowType)(1));
		this.scrolledwindow1.WindowPlacement = ((global::Gtk.CornerType)(2));
		this.vbox3.Add(this.scrolledwindow1);
		global::Gtk.Box.BoxChild w9 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.scrolledwindow1]));
		w9.Position = 1;
		// Container child vbox3.Gtk.Box+BoxChild
		this.vbox4 = new global::Gtk.VBox();
		this.vbox4.Name = "vbox4";
		this.vbox4.Spacing = 6;
		// Container child vbox4.Gtk.Box+BoxChild
		this.textBox_message = new global::Gtk.Entry();
		this.textBox_message.CanFocus = true;
		this.textBox_message.Name = "textBox_message";
		this.textBox_message.IsEditable = true;
		this.textBox_message.InvisibleChar = '●';
		this.vbox4.Add(this.textBox_message);
		global::Gtk.Box.BoxChild w10 = ((global::Gtk.Box.BoxChild)(this.vbox4[this.textBox_message]));
		w10.Position = 0;
		w10.Expand = false;
		w10.Fill = false;
		// Container child vbox4.Gtk.Box+BoxChild
		this.hbox11 = new global::Gtk.HBox();
		this.hbox11.Name = "hbox11";
		this.hbox11.Spacing = 6;
		// Container child hbox11.Gtk.Box+BoxChild
		this.hbox12 = new global::Gtk.HBox();
		this.hbox12.Name = "hbox12";
		this.hbox12.Spacing = 6;
		// Container child hbox12.Gtk.Box+BoxChild
		this.button_choose_image = new global::Gtk.Button();
		this.button_choose_image.CanFocus = true;
		this.button_choose_image.Name = "button_choose_image";
		this.button_choose_image.UseUnderline = true;
		this.button_choose_image.Label = global::Mono.Unix.Catalog.GetString("img");
		this.hbox12.Add(this.button_choose_image);
		global::Gtk.Box.BoxChild w11 = ((global::Gtk.Box.BoxChild)(this.hbox12[this.button_choose_image]));
		w11.Position = 0;
		w11.Expand = false;
		w11.Fill = false;
		// Container child hbox12.Gtk.Box+BoxChild
		this.button_choose_music = new global::Gtk.Button();
		this.button_choose_music.CanFocus = true;
		this.button_choose_music.Name = "button_choose_music";
		this.button_choose_music.UseUnderline = true;
		this.button_choose_music.Label = global::Mono.Unix.Catalog.GetString("wav");
		this.hbox12.Add(this.button_choose_music);
		global::Gtk.Box.BoxChild w12 = ((global::Gtk.Box.BoxChild)(this.hbox12[this.button_choose_music]));
		w12.Position = 1;
		w12.Expand = false;
		w12.Fill = false;
		// Container child hbox12.Gtk.Box+BoxChild
		this.button_choose_file = new global::Gtk.Button();
		this.button_choose_file.CanFocus = true;
		this.button_choose_file.Name = "button_choose_file";
		this.button_choose_file.UseUnderline = true;
		this.button_choose_file.Label = global::Mono.Unix.Catalog.GetString("file");
		this.hbox12.Add(this.button_choose_file);
		global::Gtk.Box.BoxChild w13 = ((global::Gtk.Box.BoxChild)(this.hbox12[this.button_choose_file]));
		w13.Position = 2;
		w13.Expand = false;
		w13.Fill = false;
		this.hbox11.Add(this.hbox12);
		global::Gtk.Box.BoxChild w14 = ((global::Gtk.Box.BoxChild)(this.hbox11[this.hbox12]));
		w14.Position = 0;
		w14.Expand = false;
		w14.Fill = false;
		// Container child hbox11.Gtk.Box+BoxChild
		this.hbox13 = new global::Gtk.HBox();
		this.hbox13.Name = "hbox13";
		this.hbox13.Spacing = 6;
		// Container child hbox13.Gtk.Box+BoxChild
		this.button_clear = new global::Gtk.Button();
		this.button_clear.CanFocus = true;
		this.button_clear.Name = "button_clear";
		this.button_clear.UseUnderline = true;
		this.button_clear.Label = global::Mono.Unix.Catalog.GetString("Clear");
		this.hbox13.Add(this.button_clear);
		global::Gtk.Box.BoxChild w15 = ((global::Gtk.Box.BoxChild)(this.hbox13[this.button_clear]));
		w15.Position = 0;
		w15.Expand = false;
		w15.Fill = false;
		// Container child hbox13.Gtk.Box+BoxChild
		this.button_quit = new global::Gtk.Button();
		this.button_quit.CanFocus = true;
		this.button_quit.Name = "button_quit";
		this.button_quit.UseUnderline = true;
		this.button_quit.Label = global::Mono.Unix.Catalog.GetString("Quit");
		this.hbox13.Add(this.button_quit);
		global::Gtk.Box.BoxChild w16 = ((global::Gtk.Box.BoxChild)(this.hbox13[this.button_quit]));
		w16.Position = 1;
		w16.Expand = false;
		w16.Fill = false;
		this.hbox11.Add(this.hbox13);
		global::Gtk.Box.BoxChild w17 = ((global::Gtk.Box.BoxChild)(this.hbox11[this.hbox13]));
		w17.Position = 2;
		w17.Expand = false;
		w17.Fill = false;
		this.vbox4.Add(this.hbox11);
		global::Gtk.Box.BoxChild w18 = ((global::Gtk.Box.BoxChild)(this.vbox4[this.hbox11]));
		w18.Position = 1;
		w18.Expand = false;
		w18.Fill = false;
		this.vbox3.Add(this.vbox4);
		global::Gtk.Box.BoxChild w19 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.vbox4]));
		w19.Position = 2;
		w19.Expand = false;
		w19.Fill = false;
		this.Add(this.vbox3);
		if ((this.Child != null))
		{
			this.Child.ShowAll();
		}
		this.DefaultWidth = 477;
		this.DefaultHeight = 361;
		this.Show();
		this.DeleteEvent += new global::Gtk.DeleteEventHandler(this.OnDeleteEvent);
		this.button_connect.Clicked += new global::System.EventHandler(this.ClientConnectClicked);
		this.textBox_message.KeyPressEvent += new global::Gtk.KeyPressEventHandler(this.KeyPressEvent);
		this.button_choose_image.Clicked += new global::System.EventHandler(this.img_clicked);
		this.button_choose_music.Clicked += new global::System.EventHandler(this.wav_clicked);
		this.button_choose_file.Clicked += new global::System.EventHandler(this.file_clicked);
		this.button_clear.Clicked += new global::System.EventHandler(this.ClearClicked);
		this.button_quit.Clicked += new global::System.EventHandler(this.QuitClicked);
	}
}
